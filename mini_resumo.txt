git init -> serve para iniciar repositório

git status -> serve para exibir 

git add arquivo/diretório -> serve para adicionar na área de preparação o aquivo mencionado 
git add --all = git add -> serve para adicionar todos os arquivos na área de preparação, pra depois fazer commit

git commit -m “Primeiro commit -> faz o commit e ainda aprece a seguinte descrição "Primeiro commit" no commit, serve para sabermos no que modificamos
git log -> serve para para mostrar todas as alterções feitas (histórico) em ordem cronológica
git branch -> serve para mostrar todas as branch 

git branch -d <branch_name> -> faz a remoção da branch mencionada localmente
git branch -m <nome_novo> -> serve para renomear a branch
git checkout <branch_name>  -> serve para trocar de branch
git checkout -b <branch_name> -> cria e abre na branch 


git merge <branch_name>  -> serve para juntar as ramificações do master (as branch) com a master

git clone -> serve para clonar um repositorio para a pasta que  quiser

git pull -> atualiza o repositorio local com a última versão da origem da branch remota, ou seja, com a master do git lab ou git hub por exemplo

git push -> Faz o envio das mudanças comitadas localmente para a origem da branch rastreada, ou seja, ira enviar as mudanças feitas localmente para a master do git lab ou git hub

git remote -v -> serve para fazer a listagem dos servidores remotos que o repositório está usando associados com a URL.

git remote add origin <url> -> serve para fazer  a troca da URL do servidor 'origin' para a nova URL mencionada. 
